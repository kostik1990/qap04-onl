from math import sqrt
# First task
a = 2
b = 3
c = a+b
d = a-b
e = a*b
f = a/b
print(c, d, e, f)

# Second task
x = 4
y = 3
a = abs(x)-abs(y)
b = 1+abs(x*y)
c = a/b
print(c)

# Third task
a = 18
v = a**3
print(v)

# Fourth task
a = 20
b = 10
c = (a+b)/2
d = sqrt(a*b)
print(c, d)

# Fifth task
a = 3
b = 4
c = sqrt(a*a+b*b)
s = 0.5*a*b
print(c, s)
